<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'title', 'content', 'image_url', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
