<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(3);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validateRequestData($request->all());

        if ($validator->fails()) {
            return redirect()->route('posts.create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $imageUrl = null;

        if ($request->has('image')) {
            Storage::disk('public')->putFileAs("/uploads/novosti/{$post->id}", $request->image, $request->image->getClientOriginalName());
            $imageUrl = "/uploads/novosti/{$post->id}/" . $request->image->getClientOriginalName();
        }

        Storage::disk('public')->putFileAs("/uploads/novosti/{$post->id}", $request->image, $request->image->getClientOriginalName());

        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'image_url' => $imageUrl,
            'user_id' => Auth::user()->id
        ]);

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $validator = validateRequestData($request->all());

        if ($validator->fails()) {
            return redirect()->route('posts.edit')
                            ->withErrors($validator)
                            ->withInput();
        }

        $imageUrl = null;

        if ($request->has('image')) {
            Storage::disk('public')->delete($post->image_url);
            Storage::disk('public')->putFileAs("/uploads/novosti/{$post->id}", $request->image, $request->image->getClientOriginalName());
            $imageUrl = "/uploads/novosti/{$post->id}/" . $request->image->getClientOriginalName();
        }

        Storage::disk('public')->putFileAs("/uploads/novosti/{$post->id}", $request->image, $request->image->getClientOriginalName());

        $post = Post::update([
            'title' => $request->title,
            'content' => $request->content,
            'image_url' => $imageUrl,
            // 'user_id' => Auth::user()->id
        ]);

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        Storage::disk('public')->deleteDirectory("/uploads/posts/{$post->id}");

        $post->destroy();

        return redirect()->route('posts.index');
    }

    /**
     * @param array $requstData
     * @return \Illuminate\Support\Facades\Validator
     */

    private function validateRequestData($requestData)
    {
        $messages = [
            'required'           => 'Polje \':attribute\' je obavezno.',
            'max'                => ':attribute ne smije sadržavati više od :max znakova.',
            'min'                => ':attribute ne smije sadržavati manje od :min znakova.',
        ];

        return Validator::make($requestData, [
            'title'              => 'required|string|max:255|min:10',
            'content'            => 'required|string|min:10',
            'image'              => 'required',
        ], $messages);
    }
}
